#ifndef MYSQRT_HPP_
#define MYSQRT_HPP_

#include "mathfuncs_export.h"

double MATHFUNCS_EXPORT MySqrt(double input);

#endif  // MYSQRT_HPP_
