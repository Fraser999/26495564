cmake_minimum_required(VERSION 3.0)
project(MathFuncs)

add_library(MathFuncs SHARED mysqrt.cpp mysqrt.hpp "${PROJECT_BINARY_DIR}/mathfuncs_export.h")

# Write appropriate export PP definitions in a file called 'mathfuncs_export.h' in the current binary dir
include(GenerateExportHeader)
generate_export_header(MathFuncs)

# Add the current source dir as a PUBLIC include dir (to allow mysqrt.hpp to be found by dependent targets)
# Add the current binary dir as a PUBLIC include dir (to allow mathfuncs_export.h to be found by dependent targets)
target_include_directories(MathFuncs PUBLIC ${PROJECT_SOURCE_DIR} ${PROJECT_BINARY_DIR})
