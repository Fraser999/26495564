#include "mysqrt.hpp"

#include <cmath>

double MySqrt(double input) {
  return std::sqrt(input);
}
