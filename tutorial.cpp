#include "mysqrt.hpp"
#include <iostream>

int main() {
  auto input = double{99.9};
  std::cout << "The square root of " << input << " is " << MySqrt(input) << '\n';
  return 0;
}
